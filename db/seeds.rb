# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).

AdminUser.create!(email: 'admin@example.com', password: 'password', password_confirmation: 'password') if Rails.env.development?

airplanes = Airplane.create! [
  { name: 'Ryanair', seats: 5 },
  { name: 'Alitalia', seats: 10 },
  { name: 'Lufthansa', seats: 10 }
]
flights = Flight.create! [
  { airplane: airplanes.first, destination: 'London' },
  { airplane: airplanes.second, destination: 'Miami' },
  { airplane: airplanes.third, destination: 'Tokio' }
]
flight_executions = FlightExecution.create! [
  { flight: flights.first, departure: '1-1-2021 09:00' },
  { flight: flights.second, departure: '2-1-2021 18:00' },
  { flight: flights.first, departure: '3-1-2021 09:00' },
  { flight: flights.third, departure: '4-1-2021 15:00' }
]
user = User.create! name: 'John', email: 'john@example.com'
Passenger.create! [
  { user: user, flight_execution: flight_executions.first, firstname: 'Foo', lastname: 'Bar' },
  { user: user, flight_execution: flight_executions.first, firstname: 'John', lastname: 'Doe' },
  { user: user, flight_execution: flight_executions.first, firstname: 'Joe', lastname: 'Banana', checkin: true },
  { user: user, flight_execution: flight_executions.first, firstname: 'Richard', lastname: 'Roe', checkin: true }
]
user = User.create! name: 'Max', email: 'max@example.com'
Passenger.create! [
  { user: user, flight_execution: flight_executions.first, firstname: 'Max', lastname: 'Rex' },
  { user: user, flight_execution: flight_executions.second, firstname: 'Mario', lastname: 'Bros' },
  { user: user, flight_execution: flight_executions.second, firstname: 'Jack', lastname: 'Reacher' },
  { user: user, flight_execution: flight_executions.second, firstname: 'Lau', lastname: 'Rah', checkin: true },
  { user: user, flight_execution: flight_executions.third, firstname: 'Joe', lastname: 'Banana', checkin: true }
]
