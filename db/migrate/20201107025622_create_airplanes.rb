class CreateAirplanes < ActiveRecord::Migration[5.2]
  def change
    create_table :airplanes do |t|
      t.string :name, null: false
      t.integer :seats, null: false

      t.timestamps
    end
  end
end
