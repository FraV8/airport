class CreateFlightExecutions < ActiveRecord::Migration[5.2]
  def change
    create_table :flight_executions do |t|
      t.belongs_to :flight, foreign_key: true, null: false
      t.datetime :departure, null: false

      t.timestamps
    end

    add_index :flight_executions, %i[flight_id departure], unique: true
  end
end
