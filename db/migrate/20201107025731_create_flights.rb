class CreateFlights < ActiveRecord::Migration[5.2]
  def change
    create_table :flights do |t|
      t.belongs_to :airplane, foreign_key: true, null: false
      t.string :destination, null: false

      t.timestamps
    end

    add_index :flights, %i[airplane_id destination], unique: true
  end
end
