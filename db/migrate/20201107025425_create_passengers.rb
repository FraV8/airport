class CreatePassengers < ActiveRecord::Migration[5.2]
  def change
    create_table :passengers do |t|
      t.belongs_to :user, foreign_key: true, null: false
      t.belongs_to :flight_execution, foreign_key: true, null: false
      t.string :firstname, null: false
      t.string :lastname, null: false
      t.boolean :checkin, default: false

      t.timestamps
    end
  end
end
