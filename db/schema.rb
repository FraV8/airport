# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20_201_107_032_210) do
  create_table 'admin_users', force: :cascade do |t|
    t.string 'email', default: '', null: false
    t.string 'encrypted_password', default: '', null: false
    t.string 'reset_password_token'
    t.datetime 'reset_password_sent_at'
    t.datetime 'remember_created_at'
    t.datetime 'created_at', null: false
    t.datetime 'updated_at', null: false
    t.index ['email'], name: 'index_admin_users_on_email', unique: true
    t.index ['reset_password_token'], name: 'index_admin_users_on_reset_password_token', unique: true
  end

  create_table 'airplanes', force: :cascade do |t|
    t.string 'name', null: false
    t.integer 'seats', null: false
    t.datetime 'created_at', null: false
    t.datetime 'updated_at', null: false
  end

  create_table 'flight_executions', force: :cascade do |t|
    t.integer 'flight_id', null: false
    t.datetime 'departure', null: false
    t.datetime 'created_at', null: false
    t.datetime 'updated_at', null: false
    t.index %w[flight_id departure], name: 'index_flight_executions_on_flight_id_and_departure', unique: true
    t.index ['flight_id'], name: 'index_flight_executions_on_flight_id'
  end

  create_table 'flights', force: :cascade do |t|
    t.integer 'airplane_id', null: false
    t.string 'destination', null: false
    t.datetime 'created_at', null: false
    t.datetime 'updated_at', null: false
    t.index %w[airplane_id destination], name: 'index_flights_on_airplane_id_and_destination', unique: true
    t.index ['airplane_id'], name: 'index_flights_on_airplane_id'
  end

  create_table 'passengers', force: :cascade do |t|
    t.integer 'user_id', null: false
    t.integer 'flight_execution_id', null: false
    t.string 'firstname', null: false
    t.string 'lastname', null: false
    t.boolean 'checkin', default: false
    t.datetime 'created_at', null: false
    t.datetime 'updated_at', null: false
    t.index ['flight_execution_id'], name: 'index_passengers_on_flight_execution_id'
    t.index ['user_id'], name: 'index_passengers_on_user_id'
  end

  create_table 'users', force: :cascade do |t|
    t.string 'name', null: false
    t.string 'email', null: false
    t.datetime 'created_at', null: false
    t.datetime 'updated_at', null: false
    t.index ['email'], name: 'index_users_on_email', unique: true
  end
end
