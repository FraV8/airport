Rails.env = 'test'

require_relative '../config/environment'
require 'rails/test_help'
require 'shoulda'

module ActiveSupport
  class TestCase
    fixtures :all
  end
end

Shoulda::Matchers.configure do |config|
  config.integrate do |with|
    with.test_framework :minitest
    with.library :rails
  end
end
