require 'test_helper'

class FlightExecutionTest < ActiveSupport::TestCase
  should belong_to :flight
  should have_many(:passengers)
  should validate_presence_of(:departure)
  should validate_uniqueness_of(:departure).scoped_to(:flight_id)

  test 'to' do
    assert_includes FlightExecution.to(:NOWERE), flight_executions(:one)
    refute_includes FlightExecution.to(:nowere), flight_executions(:two)
  end

  test 'on' do
    assert_includes FlightExecution.on('1-10-2021'), flight_executions(:two)
    refute_includes FlightExecution.on('1-10-2021'), flight_executions(:one)
  end

  test 'name' do
    assert_equal 'plane to nowere 🛫 01/01/2021 00:00', flight_executions(:one).name
  end

  test 'total_seats' do
    assert_equal 10, flight_executions(:one).total_seats
  end

  test 'available_seats' do
    assert_equal 9, flight_executions(:one).available_seats
  end
end
