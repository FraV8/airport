require 'test_helper'

class AirplaneTest < ActiveSupport::TestCase
  should have_many(:flights)
  should validate_presence_of(:name)
  should validate_presence_of(:seats)
  should validate_numericality_of(:seats)
    .is_greater_than(0)
    .only_integer
end
