require 'test_helper'

class UserTest < ActiveSupport::TestCase
  should validate_presence_of(:name)
  should validate_presence_of(:email)
  should validate_uniqueness_of(:email).case_insensitive

  test 'email format' do
    user = users :one
    user.email = 'asd'
    refute user.valid?
    assert_equal ['is invalid'], user.errors[:email]
  end
end
