require 'test_helper'

class PassengerTest < ActiveSupport::TestCase
  should belong_to :user
  should belong_to :flight_execution
  should validate_presence_of(:firstname)
  should validate_presence_of(:lastname)

  test 'checked' do
    assert_includes Passenger.checked, passengers(:three)
    refute_includes Passenger.checked, passengers(:one)
  end

  test 'unchecked' do
    assert_includes Passenger.unchecked, passengers(:two)
    refute_includes Passenger.unchecked, passengers(:three)
  end

  test 'fullname' do
    assert_equal 'foo bar', passengers(:one).fullname
  end

  test 'checkin!' do
    p = passengers(:one)
    p.checkin!
    assert p.checkin
  end

  test 'valdidate max passengers' do
    airplane = airplanes :one
    airplane.seats = 1
    airplane.save
    passenger = passengers :two
    passenger.flight_execution = flight_executions :one
    refute passenger.valid?
    assert_equal ['the flight is full!'], passenger.errors[:flight_execution]
  end
end
