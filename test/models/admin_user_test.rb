require 'test_helper'

class AdminUserTest < ActiveSupport::TestCase
  should validate_presence_of(:email)
  should validate_uniqueness_of(:email).case_insensitive
end
