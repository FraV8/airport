require 'test_helper'

class FlightTest < ActiveSupport::TestCase
  should belong_to :airplane
  should have_many(:flight_executions)
  should validate_presence_of(:destination)
  should validate_uniqueness_of(:destination).scoped_to(:airplane_id)

  test 'name' do
    assert_equal 'plane to nowere', flights(:one).name
  end
end
