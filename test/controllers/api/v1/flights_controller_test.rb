module Api
  module V1
    class FlightsControllerTest < ActionController::TestCase
      test 'departures with valid params' do
        get :departures, params: {
          destination: flights(:one).destination,
          departure: flight_executions(:one).departure,
          passengers: 2
        }
        assert_response :success

        result = JSON.parse(response.body)['data'].first
        f = flight_executions :one
        assert_equal f.id.to_s, result['id']
        assert_equal f.flight.airplane.name, result['attributes']['airplane']
        assert_equal f.flight.destination, result['attributes']['destination']
        assert_equal f.departure, result['attributes']['departure']
      end

      test 'departures with no results' do
        get :departures, params: {
          destination: :everest,
          departure: flight_executions(:one).departure,
          passengers: 2
        }
        assert_response :success

        res = JSON.parse response.body
        assert_empty res['data']
        assert_nil res['error']
      end

      test 'departures with invalid params' do
        get :departures, params: nil
        assert_response :bad_request

        res = JSON.parse response.body
        assert_nil res['data']
        assert_not_nil res['error']
      end
    end
  end
end
