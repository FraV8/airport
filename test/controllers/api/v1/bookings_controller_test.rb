module Api
  module V1
    class BookingsControllerTest < ActionController::TestCase
      include Devise::Test::ControllerHelpers
      include ActionMailer::TestHelper

      test 'checkin with valid params' do
        passenger = passengers :one

        post :checkin, params: {
          passenger: passenger
        }

        assert passenger.reload.checkin
        assert_response :created
        assert_enqueued_emails 1
      end

      test 'checkin not authorized' do
        passenger = passengers :two

        post :checkin, params: {
          passenger: passenger
        }

        refute passenger.reload.checkin
        assert_response :bad_request
        res = JSON.parse response.body
        assert_equal 'not authorized', res['error']
        assert_no_enqueued_emails
      end

      test 'checkin already done' do
        passenger = passengers :three

        post :checkin, params: {
          passenger: passenger
        }

        assert_response :bad_request
        res = JSON.parse response.body
        assert_equal 'checkin already done', res['error']
        assert_no_enqueued_emails
      end
    end
  end
end
