module Api
  module V1
    class PassengersControllerTest < ActionController::TestCase
      include Devise::Test::ControllerHelpers
      include ActionMailer::TestHelper

      test 'book with valid params' do
        assert_difference('Passenger.count', 2) do
          post :book, params: {
            flight: flight_executions(:one),
            passengers: [
              { firstname: 'Al', lastname: 'Do' },
              { firstname: 'Ma', lastname: 'Rio' }
            ]
          }
        end
        assert_response :created
        assert_enqueued_emails 1
      end

      test 'book with not enough seats on the plane' do
        assert_no_difference('Passenger.count') do
          post :book, params: {
            flight: flight_executions(:two),
            passengers: [
              { firstname: 'Al', lastname: 'Do' },
              { firstname: 'Ma', lastname: 'Rio' }
            ]
          }
        end
        assert_response :bad_request
        assert_no_enqueued_emails

        res = JSON.parse response.body
        assert_equal 'insufficient seats available', res['error']
      end

      test 'book with invalid params' do
        assert_no_difference('Passenger.count') do
          post :book, params: {
            flight: flight_executions(:one),
            passengers: [
              { firstname: 'Al' }
            ]
          }
        end
        assert_response :bad_request
        assert_no_enqueued_emails

        res = JSON.parse response.body
        assert_not_nil res['error']
      end
    end
  end
end
