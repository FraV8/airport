class UserMailer < ApplicationMailer
  def book_confirm(user, flight, passengers)
    @user = user
    @flight = flight
    @passengers = passengers
    mail subject: 'Flight Booking Confirmed', to: user.email
  end

  def checkin_confirm(passenger)
    @passenger = passenger
    mail subject: 'Flight Boarding Pass', to: passenger.user.email
  end

  def remind_checkin(passenger)
    @passenger = passenger
    mail subject: 'Checkin Reminder', to: passenger.user.email
  end
end
