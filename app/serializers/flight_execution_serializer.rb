class FlightExecutionSerializer < ActiveModel::Serializer
  attributes :id, :airplane, :destination, :departure

  def airplane
    object.flight.airplane.name
  end

  def destination
    object.flight.destination
  end
end
