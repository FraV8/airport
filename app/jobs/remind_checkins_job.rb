class RemindCheckinsJob < ApplicationJob
  queue_as :default

  def perform(flight)
    FlightExecution.find(flight).passengers.unchecked.each do |passenger|
      UserMailer.remind_checkin(passenger).deliver_now
    end
  end
end
