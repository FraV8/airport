ActiveAdmin.register Flight do
  menu priority: 3

  permit_params :airplane_id, :destination

  form do |f|
    f.object.airplane_id ||= params[:airplane]
    f.inputs do
      f.input :airplane
      f.input :destination
    end
    f.actions
  end

  show do
    attributes_table do
      row :airplane
      row :destination
      row :created_at
      row :updated_at
    end

    panel :FlightExecutions do
      table_for flight.flight_executions do
        column :departure
        column :total_seats
        column :available_seats
        column do |flight_execution|
          span link_to 'View', admin_flight_execution_path(flight_execution), class: 'button'
          span link_to 'Edit', edit_admin_flight_execution_path(flight_execution), class: 'button'
        end
      end
      span link_to 'New Flight Execution', new_admin_flight_execution_path(flight: flight), class: 'button'
    end
  end
end
