ActiveAdmin.register User do
  menu priority: 6

  permit_params :name, :email

  show do
    attributes_table do
      row :name
      row :email
      row :created_at
      row :updated_at
    end

    panel :Passengers do
      table_for user.passengers do
        column :flight_execution
        column :firstname
        column :lastname
        column :checkin
        column :created_at
        column do |passenger|
          span link_to 'View', admin_passenger_path(passenger), class: 'button'
          span link_to 'Edit', edit_admin_passenger_path(passenger), class: 'button'
        end
      end
      span link_to 'New Passenger', new_admin_passenger_path(user: user), class: 'button'
    end
  end
end
