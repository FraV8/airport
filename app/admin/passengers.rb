ActiveAdmin.register Passenger do
  menu priority: 5

  permit_params :user_id, :flight_execution_id, :firstname, :lastname, :checkin

  form do |f|
    f.object.user_id ||= params[:user]
    f.object.flight_execution_id ||= params[:flight_execution]
    f.inputs do
      f.input :user, input_html: { disabled: f.object.persisted? }
      f.input :flight_execution, input_html: { disabled: f.object.persisted? }
      f.input :firstname
      f.input :lastname
      f.input :checkin
    end
    f.actions
  end
end
