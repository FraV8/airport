ActiveAdmin.register_page 'Dashboard' do
  menu priority: 1, label: proc { I18n.t('active_admin.dashboard') }

  breadcrumb { nil }

  page_action :send_reminders do
    RemindCheckinsJob.perform_later params.require :flight
    redirect_to root_path, notice: 'Ok!'
  end

  content title: 'Dashboard' do
    columns do
      column do
        panel 'Upcoming Flights' do
          table_for FlightExecution.where("departure between date() and date('now','+1 month')").order('departure') do
            column(:flight) { |f| link_to(f.name, admin_flight_execution_path(f)) }
            column(:available_seats, &:available_seats)
            column(:passengers) { |f| f.passengers.size }
            column(:checkins) do |f|
              checkins = f.passengers.checked.size
              if f.passengers.size.nonzero?
                span class: "status_tag #{checkins == f.passengers.size ? 'yes' : 'no'}" do
                  checkins
                end
              else
                0
              end
            end
            column(:action) do |f|
              if f.passengers.unchecked.present?
                span link_to 'Send Reminders', admin_dashboard_send_reminders_path(flight: f), class: 'button', data: {
                  confirm: 'Send a reminder email to everyone who has not checked-in?'
                }
              end
            end
          end
        end
      end
      column do
        panel 'Last Bookings' do
          table_for Passenger.order('created_at desc').limit(20) do
            column(:fullname) { |p| link_to(p.fullname, admin_passenger_path(p)) }
            column(:created_at) { |p| p.created_at.strftime('%d/%m/%Y %H:%M') }
            column(:checkin, &:checkin)
          end
        end
      end
    end
  end
end
