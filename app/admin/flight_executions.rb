ActiveAdmin.register FlightExecution do
  menu priority: 4

  permit_params :flight_id, :departure

  form do |f|
    f.object.flight_id ||= params[:flight]
    f.inputs do
      f.input :flight, input_html: { disabled: f.object.persisted? }
      f.input :departure, as: :datetime_picker,
                          datepicker_options: {
                            min_date: 1.days.from_now.to_date
                          }
    end
    f.actions
  end

  index do
    id_column
    column :flight
    column :departure
    column :total_seats
    column :available_seats
    column :passengers do |f|
      f.passengers.size
    end
    actions
  end

  show do
    attributes_table do
      row :flight
      row :departure
      row :total_seats
      row :available_seats
    end
    panel :Passengers do
      table_for flight_execution.passengers do
        column :user
        column :firstname
        column :lastname
        column :checkin
        column :created_at
        column do |passenger|
          span link_to 'View', admin_passenger_path(passenger), class: 'button'
          span link_to 'Edit', edit_admin_passenger_path(passenger), class: 'button'
        end
      end
      span link_to 'New Passenger', new_admin_passenger_path(flight_execution: flight_execution), class: 'button'
    end
  end
end
