ActiveAdmin.register Airplane do
  menu priority: 2
  permit_params :name, :seats

  show do
    attributes_table do
      row :name
      row :seats
      row :created_at
      row :updated_at
    end

    panel :Flights do
      table_for airplane.flights do
        column :destination
        column :created_at
        column do |flight|
          span link_to 'View', admin_flight_path(flight), class: 'button'
          span link_to 'Edit', edit_admin_flight_path(flight), class: 'button'
        end
      end
      span link_to 'New Flight', new_admin_flight_path(airplane: airplane), class: 'button'
    end
  end
end
