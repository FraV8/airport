module Api
  module V1
    class FlightsController < ApplicationController
      def departures
        results =
          FlightExecution.to(params.require(:destination))
                         .on(params.require(:departure)).select do |f|
            f.available_seats >= params.require(:passengers).to_i
          end

        render json: results, adapter: :json_api
      end
    end
  end
end
