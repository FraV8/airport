module Api
  module V1
    class PassengersController < ApplicationController
      def book
        flight = FlightExecution.find params.require(:flight)
        passengers = params.permit(passengers: %i[firstname lastname]).require :passengers

        raise 'insufficient seats available' if flight.available_seats < passengers.size

        passengers.each do |p|
          p.merge! user: current_user, flight_execution: flight
        end
        bookers = Passenger.create! passengers

        UserMailer.book_confirm(current_user, flight, bookers).deliver_later

        head :created
      end
    end
  end
end
