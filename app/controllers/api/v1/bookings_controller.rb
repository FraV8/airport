module Api
  module V1
    class BookingsController < ApplicationController
      def checkin
        passenger = Passenger.find params.require(:passenger)

        raise 'not authorized' if passenger.user != current_user
        raise 'checkin already done' if passenger.checkin

        passenger.checkin!

        UserMailer.checkin_confirm(passenger).deliver_later

        head :created
      end
    end
  end
end
