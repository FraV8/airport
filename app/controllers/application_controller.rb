class ApplicationController < ActionController::Base
  protect_from_forgery

  rescue_from StandardError do |e|
    render json: { error: e }, status: :bad_request
  end

  # TODO: take authenticated user from session cookie
  def current_user
    User.find 1
  end
end
