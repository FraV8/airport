class Airplane < ApplicationRecord
  has_many :flights, dependent: :restrict_with_error

  validates :name, :seats,
            presence: true

  validates :seats, numericality: {
    only_integer: true,
    greater_than: 0
  }
end
