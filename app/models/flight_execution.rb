class FlightExecution < ApplicationRecord
  belongs_to :flight
  has_many :passengers, dependent: :restrict_with_error

  validates :departure, presence: true
  validates_uniqueness_of :departure, scope: :flight_id

  scope :to, lambda { |destination|
    joins(:flight).merge Flight.where 'lower(destination) = lower(?)', destination
  }

  scope :on, lambda { |departure|
    where('departure > ?', Time.now + 4.hour) # minimum interval for booking and checkin
      .where('departure > ?', Date.parse(departure))
      .order :departure
  }

  def name
    "#{flight.name} 🛫 #{departure.strftime('%d/%m/%Y %H:%M')}"
  rescue StandardError
    nil
  end

  def total_seats
    flight.airplane.seats
  end

  def available_seats
    total_seats - passengers.size
  end
end
