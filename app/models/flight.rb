class Flight < ApplicationRecord
  belongs_to :airplane
  has_many :flight_executions, dependent: :restrict_with_error

  validates :destination, presence: true
  validates_uniqueness_of :destination, scope: :airplane_id

  def name
    "#{airplane.name} to #{destination}"
  rescue StandardError
    nil
  end
end
