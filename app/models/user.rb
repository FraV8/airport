class User < ApplicationRecord
  has_many :passengers, dependent: :restrict_with_error

  validates :name, :email,
            presence: true

  validates :email,
            uniqueness: { case_sensitive: false },
            format: { with: URI::MailTo::EMAIL_REGEXP }
end
