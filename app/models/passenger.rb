class Passenger < ApplicationRecord
  belongs_to :user
  belongs_to :flight_execution

  validates :firstname, :lastname,
            presence: true

  validate :max_passengers, if: :flight_execution_id_changed?

  scope :checked, -> { where(checkin: true) }
  scope :unchecked, -> { where.not(checkin: true) }

  def fullname
    "#{firstname} #{lastname}"
  end

  def checkin!
    update_attribute :checkin, true
  end

  private

  def max_passengers
    return unless flight_execution

    errors.add :flight_execution, 'the flight is full!' unless flight_execution.available_seats.positive?
  end
end
