Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes self

  namespace :api do
    namespace :v1 do
      get '/departures', to: 'flights#departures'
      post '/book', to: 'passengers#book'
      post '/checkin', to: 'bookings#checkin'
    end
  end

  root to: redirect('/admin')
end
