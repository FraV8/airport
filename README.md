# Airport 🛫

_maybe the simplest ever_

[![Ruby Style Guide](https://img.shields.io/badge/code_style-rubocop-brightgreen.svg)](https://github.com/rubocop-hq/rubocop)

- Ruby 2.6.6
- Rails 5.2.4.4

## quickstart

setup db and start the server:

```sh
rails db:migrate
rails db:seed
rails server
```

open [http://localhost:3000/admin](http://localhost:3000/admin)

login as default admin:

> email: admin@example.com
>
> password: password

## API

### to get the list of bookable flights

`GET /api/v1/departures`

params:

- destination: where to fly
- departure: take-off date
- passengers: how many people

example:

http://localhost:3000/api/v1/departures?destination=miami&departure=1-1-2021&passengers=2

### to book a flight

`POST /api/v1/book`

params:

- flight: id of the flight execution taken from the results of the previous API
- passengers: list of the people to book the flight for, with first and last names

example:

```sh
POST http://localhost:3000/api/v1/book
params: {
   "flight": 1,
   "passengers": [
      {"firstname":"Al", "lastname":"Do"},
      {"firstname":"Ma", "lastname":"Rio"}
    ]
}
```

### to check-in an owned existing passenger

`POST /api/v1/checkin`

params:

- passenger: id of the passenger to check-in

example:

```sh
POST http://localhost:3000/api/v1/checkin
params: {
   "passenger": 1,
}
```

## testing

run tests:

```sh
rails test
```

run Ruby linter:

```sh
rubocop
```
